package com.bank.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;

@Component
public class ErrorFilter implements Filter {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			boolean ignoreAuth = true;
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			HttpSession session = req.getSession(false);
			res.setHeader("Access-Control-Allow-Origin", "*");
			res.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
			res.setHeader("Access-Control-Allow-Methods", "GET,POST,PATCH,DELETE,PUT,OPTIONS");
			res.setHeader("Access-Control-Allow-Headers", "*");
			res.setHeader("Access-Control-Max-Age", "86400");
			if (session == null) {
				if (ignoreAuth || req.getServletPath().contains("/user/login")) {
					chain.doFilter(request, response);
				} else {
					throw new ServletException("UnAuthorized!");
				}
			} else {
				chain.doFilter(request, response);
			}
		} catch (Exception e) {
			System.out.println(e);
			throw e;
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}
}
