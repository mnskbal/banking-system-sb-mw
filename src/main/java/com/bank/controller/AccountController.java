package com.bank.controller;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.exception.BankingException;
import com.bank.model.dto.AccountDto;
import com.bank.service.AccountService;

import io.swagger.v3.oas.annotations.responses.ApiResponse;

@RestController
@RequestMapping(value = "/api/user/account")
public class AccountController {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private AccountService accountService;
	
	@ApiResponse
	@GetMapping(value = "/all", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Collection<AccountDto> getAllAccountsOfUser(HttpServletRequest req,
			@RequestParam(name = "uId", required = true) Long uId) throws BankingException {
		logger.debug("Method entry");
		List<AccountDto> accounts = Collections.emptyList();
		try {
			if (uId > 0)
				accounts = accountService.getAccounts(uId);
			logger.debug("Successfully retrieved accounts");
		} catch (Exception e) {
			throw new BankingException("Unable to serve the request!");
		}
		logger.debug("Method exit");
		return accounts;
	}
	
	@GetMapping(value = "/fetch", produces = { MediaType.APPLICATION_JSON_VALUE })
	public Collection<AccountDto> fetchAllAccountsOfUserFromSource(HttpServletRequest req,
			@RequestParam(name = "uId", required = true) Long uId) throws BankingException {
		logger.debug("Method entry");
		List<AccountDto> accounts = Collections.emptyList();
		try {
			if (uId > 0)
				accounts = accountService.fetchAccounts(uId);
			logger.debug("Successfully retrieved accounts");
		} catch (Exception e) {
			logger.error("Exception Occured!. Details: {}", e.getMessage(), e);
			throw new BankingException("Unable to serve the request!");
		}
		logger.debug("Method exit");
		return accounts;
	}
	
}
