package com.bank.controller;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bank.exception.BankingException;
import com.bank.service.AccountService;

@RestController
@RequestMapping(value = "/api/transactions")
public class TransactionController {

	@Autowired
	private AccountService accountService;

	@GetMapping
	public void getTransactions(HttpServletRequest req, HttpServletResponse resp) throws BankingException {
		try {
			String accountId = req.getParameter("account-id");
			if (StringUtils.isBlank(accountId) || !NumberUtils.isCreatable(accountId))
				throw new BankingException("A Valid account identifier is required to access this end-point!");
			req.setAttribute("transactions", null);
			req.getRequestDispatcher("/display-transactions.jsp").forward(req, resp);
		} catch (BankingException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException("Unable to fullfill the request!");
		}
	}

	@GetMapping(value = "/deposit")
	public boolean deposit(@RequestParam(name = "account-id") Long accountId,
			@RequestParam(name = "amount") BigDecimal amount) throws BankingException {
		try {
			if (accountId <= 0 || (amount.compareTo(BigDecimal.ZERO) <= 0))
				throw new BankingException(
						"A Valid account identifier and amount are required to access this end-point!");
			return accountService.deposit(accountId, amount);
		} catch (BankingException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException("Unable to fullfill the request!");
		}
	}

	@GetMapping(value = "/withdraw")
	public boolean withdraw(@RequestParam(name = "account-id") Long accountId,
			@RequestParam(name = "amount") BigDecimal amount) throws BankingException {
		try {
			if (accountId <= 0 || (amount.compareTo(BigDecimal.ZERO) <= 0))
				throw new BankingException(
						"A Valid account identifier and amount are required to access this end-point!");
			return accountService.withdraw(accountId, amount);
		} catch (BankingException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new BankingException("Unable to fullfill the request!");
		}
	}
}
