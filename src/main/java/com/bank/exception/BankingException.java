package com.bank.exception;

import javax.servlet.ServletException;

import com.bank.enums.ErrorCode;

public class BankingException extends ServletException {
	private static final long serialVersionUID = 1L;

	private final ErrorCode errorCode;

	public BankingException(ErrorCode errorCode) {
		super();
		this.errorCode = errorCode;
	}

	public BankingException(ErrorCode errorCode, String message, Throwable cause) {
		super(message, cause);
		this.errorCode = errorCode;
	}

	public BankingException(String message, Throwable cause) {
		super(message, cause);
		this.errorCode = ErrorCode.UNKNOWN;
	}

	public BankingException(String message) {
		super(message);
		this.errorCode = ErrorCode.UNKNOWN;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	@Override
	public String toString() {
		return "BankingException [errorCode=" + errorCode + ", errorMessage=" + errorCode.getDescription() + "]";
	}

}
