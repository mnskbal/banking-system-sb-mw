package com.bank.exception;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class BankingSystemExceptionHandler {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@ExceptionHandler(BankingException.class)
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public Map<String, Object> handleBankingException(BankingException e) {
		logger.error("Exception Occured!. Details: {}", e.getMessage(), e);
		Map<String, Object> resp = new HashMap<>();
		resp.put("status", false);
		resp.put("message", e.getMessage());
		resp.put("error-code", e.getErrorCode());
		return resp;
	}

}
