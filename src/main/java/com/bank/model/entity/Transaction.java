package com.bank.model.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.bank.enums.TransactionType;

@Entity
@Table(name = "transaction")
public class Transaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private BigDecimal amount;
	
	@Enumerated(EnumType.STRING)
	private TransactionType type;

	private LocalDateTime date;

	@Column(name = "account_id")
	private Long accountId;

	public Transaction() {
		super();
	}

	public Transaction(BigDecimal amount, TransactionType type, Date date, Long accountId) {
		super();
		this.amount = amount;
		this.type = type;
		this.date = LocalDateTime.now();
		this.accountId = accountId;
	}

	public Transaction(Long id, BigDecimal amount, TransactionType type, Date date, Long accountId) {
		super();
		this.id = id;
		this.amount = amount;
		this.type = type;
		this.date = LocalDateTime.now();
		this.accountId = accountId;
	}

	public Long getId() {
		return id;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public TransactionType getType() {
		return type;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Long getAccountId() {
		return accountId;
	}

	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		Transaction o = (Transaction) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "Transaction [id=" + id + ", amount=" + amount + ", type=" + type + ", date=" + date + ", accountId="
				+ accountId + "]";
	}

}
