package com.bank.model.dto;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.bank.model.entity.User;

public class UserDto {

	private Long id;
	private String firstName;
	private String lastName;
	private String password;
	private Set<AccountDto> accounts;

	public UserDto() {
		super();
	}

	public UserDto(Long id, String firstName, String lastName, String password, Set<AccountDto> accounts) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.accounts = accounts;
		this.password = password;
	}

	public UserDto(User user) {
		super();
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.password = user.getPassword();
		if (CollectionUtils.isNotEmpty(user.getAccounts()))
			this.accounts = user.getAccounts().stream().filter(Objects::nonNull).map(AccountDto::new)
					.collect(Collectors.toSet());
	}

	public Long getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<AccountDto> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<AccountDto> accounts) {
		this.accounts = accounts;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		UserDto o = (UserDto) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
