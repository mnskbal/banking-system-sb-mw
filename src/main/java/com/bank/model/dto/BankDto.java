package com.bank.model.dto;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;

import com.bank.model.entity.Bank;

public class BankDto {

	private Long id;
	private String name;
	private Set<AccountDto> accounts;

	public BankDto() {
		super();
	}

	public BankDto(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public BankDto(Long id, String name, Set<AccountDto> accounts) {
		super();
		this.id = id;
		this.name = name;
		this.accounts = accounts;
	}

	public BankDto(Bank bank) {
		super();
		this.id = bank.getId();
		this.name = bank.getName();
		if (CollectionUtils.isNotEmpty(bank.getAccounts()))
			this.accounts = bank.getAccounts().stream().filter(Objects::nonNull).map(AccountDto::new)
					.collect(Collectors.toSet());
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<AccountDto> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<AccountDto> accounts) {
		this.accounts = accounts;
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (this.getClass() != obj.getClass())
			return false;
		BankDto o = (BankDto) obj;
		return this.id.equals(o.getId());
	}

	@Override
	public String toString() {
		return "Bank [id=" + id + ", name=" + name + "]";
	}

}
