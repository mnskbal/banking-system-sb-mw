package com.bank.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.entity.Transaction;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long>{

	@Query(value = "FROM Transaction WHERE accountId = :accountId")
	public Set<Transaction> findAllTransactions(Long accountId);

}
