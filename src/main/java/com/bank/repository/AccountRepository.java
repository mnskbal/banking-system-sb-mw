package com.bank.repository;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bank.model.entity.Account;


@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

//	@Query(value = "FROM Account a JOIN FETCH a.transactions JOIN FETCH a.bank b JOIN FETCH b.accounts WHERE a.userId = :userId")
//	public List<Account> listAccounts(Long userId);
	
//	@Query(value = "SELECT * FROM account WHERE user_id = :userId", nativeQuery = true)
//	public List<Account> listAccounts(Long userId);
	
	@EntityGraph(attributePaths = {"transactions", "bank", "bank.accounts"})
	public List<Account> findByUserId(Long userId);
		
	@Modifying
	@Query(value = "UPDATE Account SET balance = :balance, bankId = :bankId, userId = :userId WHERE id = :accountId")
	public int updateAccount(Long accountId, BigDecimal balance, Long bankId, Long userId);

	@Modifying
	@Query(value = "UPDATE Account SET balance = :balance WHERE id = :accountId")
	public int updateAccountBalance(Long accountId, BigDecimal balance);

}
