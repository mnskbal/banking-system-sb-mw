package com.bank.service;

import java.time.LocalDateTime;
import java.util.concurrent.CompletableFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
	private Logger logger = LoggerFactory.getLogger(getClass());

	@Async("notifyExecutor")
	public CompletableFuture<Boolean> notifyUser() {
		logger.info("Entered Async block!");
		try {
			Thread.sleep(5000);
			logger.info("T-NAME: " + Thread.currentThread().getName() + ". TG-NAME: "
					+ Thread.currentThread().getThreadGroup().getName());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		logger.info("Exited Async block!");
		return CompletableFuture.completedFuture(true);
	}

	@Scheduled(cron = "0/10 * * * * *")
	public void doScheduledJob() {
		logger.info("Job invoked!. Time:{} ", LocalDateTime.now());
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron = "0/10 * * * * *")
	public void doScheduledJob1() {
		logger.info("Job-2 invoked!. Time:{} ", LocalDateTime.now());
	}

}
