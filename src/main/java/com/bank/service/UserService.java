package com.bank.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bank.exception.BankingException;
import com.bank.model.dto.UserDto;
import com.bank.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public UserDto getUser(Long userId) throws BankingException {
		return new UserDto(userRepository.findById(userId)
				.orElseThrow(() -> new BankingException("Unable to find any users with the given identifier!")));
	}
}
