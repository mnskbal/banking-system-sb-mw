package com.bank.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.bank.enums.ErrorCode;
import com.bank.enums.TransactionType;
import com.bank.exception.BankingException;
import com.bank.model.dto.AccountDto;
import com.bank.model.dto.BankDto;
import com.bank.model.dto.TransactionDto;
import com.bank.model.entity.Account;
import com.bank.model.entity.Bank;
import com.bank.repository.AccountRepository;
import com.bank.repository.BankRepository;

@Service
public class AccountService {

	Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private AsyncService asyncService;

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private BankRepository bankRepository;
	@Autowired
	private TransactionService transactionService;

	public BigDecimal enquireBalance(Long accountId) throws BankingException {
		return getAccount(accountId).getBalance();
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public boolean withdraw(Long accountId, BigDecimal amount) throws BankingException {
		boolean status = false;
		Account acc = getAccount(accountId);
		if (acc.getBalance().compareTo(amount) > 0) {
			status = accountRepository.updateAccountBalance(accountId, acc.getBalance().subtract(amount)) > 0
					&& transactionService.createTransaction(accountId, amount, TransactionType.D);
			CompletableFuture<Boolean> notifyStatusFuture = asyncService.notifyUser();
			try {
				logger.info("notification status: {}", notifyStatusFuture.get());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ExecutionException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			throw new BankingException(ErrorCode.INSUFFICIENT_BALANCE);
		}
		return status;
	}

	@Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public boolean deposit(Long accountId, BigDecimal amount) throws BankingException {
		Account acc = getAccount(accountId);
		try {
			boolean status = accountRepository.updateAccountBalance(accountId, acc.getBalance().add(amount)) > 0
					&& transactionService.createTransaction(accountId, amount, TransactionType.C);
			int limit = 10;			
			List<CompletableFuture<?>> allFutures = new ArrayList<>();
			for (int j = 0; j < limit; j++) {
				CompletableFuture<Boolean> notifyStatusFuture = asyncService.notifyUser();
				allFutures.add(notifyStatusFuture);
			}
			
			CompletableFuture.allOf(allFutures.toArray(new CompletableFuture[0])).join();
			
//			logger.info("notification status: {}", notifyStatusFuture.get());
			return status;
		} catch (DataAccessException e) {
			logger.error("Data Access Exception Occured. Details: {}", e.getMessage(), e);
			throw new BankingException(ErrorCode.UNKNOWN);
		} catch (Exception e) {
			logger.error("Exception Occured. Details: {}", e.getMessage(), e);
			throw new BankingException(ErrorCode.UNKNOWN);
		}
	}

	public Account getAccount(Long accountId) throws BankingException {
		try {
			return accountRepository.findById(accountId)
					.orElseThrow(() -> new BankingException(ErrorCode.ACCOUNT_NOT_FOUND));
		} catch (DataAccessException e) {
			logger.error("Data Access Exception Occured. Details: {}", e.getMessage(), e);
			throw new BankingException(ErrorCode.UNKNOWN);
		} catch (Exception e) {
			logger.error("Exception Occured. Details: {}", e.getMessage(), e);
			throw new BankingException(ErrorCode.UNKNOWN);
		}
	}

	public List<AccountDto> getAccounts(Long userId) throws BankingException {
		List<AccountDto> accountDtos = Collections.emptyList();
		List<Account> accounts = accountRepository.findByUserId(userId);
		if (CollectionUtils.isNotEmpty(accounts)) {
			accountDtos = accounts.stream().filter(Objects::nonNull).map(AccountDto::new).map(a -> {
				try {
					System.out.println(a.getTransactions());
					BankDto bankDto = a.getBank();
					if (bankDto == null) {
						Bank bank = bankRepository.findById(a.getBankId()).orElse(null);
						if (bank != null)
							a.setBank(new BankDto(bank));
					}
					if (CollectionUtils.isEmpty(a.getTransactions())) {
						Set<TransactionDto> transactionDtos = transactionService.fetchAllTransactionDtos(a.getId());
						if (CollectionUtils.isNotEmpty(transactionDtos))
							a.setTransactions(transactionDtos);
					}
				} catch (BankingException e) {
					e.printStackTrace();
				}
				return a;
			}).filter(Objects::nonNull).collect(Collectors.toList());
		}
		return accountDtos;
	}

	// feign client
	public List<AccountDto> fetchAccounts(Long userId) throws BankingException {
		List<AccountDto> accountDtos = Collections.emptyList();

		RestTemplate rs = new RestTemplate();
		UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080")
				.path("/banking-system-mw/api/user/account/all").queryParam("uId", userId);
		ResponseEntity<List<AccountDto>> apiResp = rs.exchange(uriBuilder.build().toUri(), HttpMethod.GET, null,
				new ParameterizedTypeReference<List<AccountDto>>() {
				});
		if (apiResp.getStatusCode().is2xxSuccessful() && apiResp.hasBody()) {
			System.out.println(apiResp.getBody());
			accountDtos = apiResp.getBody();
		}
		return accountDtos;
	}

}
