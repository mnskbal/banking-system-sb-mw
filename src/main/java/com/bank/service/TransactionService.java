package com.bank.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bank.enums.TransactionType;
import com.bank.exception.BankingException;
import com.bank.model.dto.TransactionDto;
import com.bank.model.entity.Transaction;
import com.bank.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	private TransactionRepository transactionRepository;

	public Set<TransactionDto> fetchAllTransactionDtos(Long accountId) throws BankingException {
		Set<Transaction> transactions = fetchAllTransactions(accountId);
		return CollectionUtils.isNotEmpty(transactions)
				? transactions.stream().filter(Objects::nonNull).map(TransactionDto::new).collect(Collectors.toSet())
				: Collections.emptySet();
	}

	public Set<Transaction> fetchAllTransactions(Long accountId) throws BankingException {
		return transactionRepository.findAllTransactions(accountId);
	}
	
	@Transactional(rollbackFor = Exception.class)
	public boolean createTransaction(Long accountId, BigDecimal amount, TransactionType type) throws BankingException {
		return transactionRepository.save(new Transaction(amount, type, new Date(), accountId)) != null;
	}

}
